# Alto Gamer Game Reviews Collector
##### An extensible games review collector framework

## What is Alto Gamer Game Reviews Collector?
Alto Gamer Game Reviews Collector is a Java web crawler that collects games
reviews from popular videogames websites. It also is very extensible,
and supports custom parsers for any website.

## License
Alto Gamer Game Reviews Collector is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at Mozilla Public License FAQ.