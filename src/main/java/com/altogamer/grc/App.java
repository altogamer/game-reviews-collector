/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

import com.altogamer.grc.impl.SimpleProcessor;
import com.altogamer.grc.impl.metacritic.MetacriticCollector;
import com.altogamer.grc.impl.metacritic.MetacriticPcAllUrlIterator;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for running a Spider quickly. Useful for quick testing and benchmarking.
 *
 * Benchmarks: CorePoolSize (CPS) / MaximumPoolSize (MPS)
 *     CPS=10, MPS=10 : 9592 games collected in 85021ms
 *     CPS=15, MPS=15 : 9592 games collected in 85023ms
 *     CPS=20, MPS=20 : 9592 games collected in 90023ms
 *     CPS=25, MPS=25 : 9592 games collected in 90023ms
 *     CPS=30, MPS=30 : 9592 games collected in 100026ms
 *     CPS=40, MPS=40 : 9592 games collected in 125027ms
 */
public class App {
    private static final Logger logger = LoggerFactory.getLogger(WebCrawler.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        WebCrawler crawler = new WebCrawler();
        crawler.setUrlIterator(new MetacriticPcAllUrlIterator());
        crawler.setCollector(new MetacriticCollector());
        crawler.setProcessor(new SimpleProcessor());

        crawler.setCorePoolSize(30);
        crawler.setMaximumPoolSize(30);

        crawler.run();
    }
}
