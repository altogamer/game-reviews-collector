/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

import java.io.IOException;
import java.util.List;

/**
 * Extracts and returns game information from a given content.
 */
public interface Collector {

    /** Parses the given content and returns the games found. */
    List<GameScore> collect(String content) throws IOException;

}
