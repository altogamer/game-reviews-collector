/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

import java.util.List;

/**
 * Events from a PageCollector.
 */
public interface PageCollectorListener {

    /**
     * This method is called when a PageCollector finishes its work.
     * @param games The list of games that were collected from the url.
     * @param url the URL that was fetched to collect games.
     */
    void onComplete(List<GameScore> games, Url url);

}
