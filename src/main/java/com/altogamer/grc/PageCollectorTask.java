/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

import com.altogamer.grc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A process unit to collect games from an specific URL using an specific Collector.
 * This class is designed to be run as a thread, maybe using a BaseSpider.
 */
public class PageCollectorTask implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(PageCollectorTask.class);

    private Url url;
    private Collector collector;
    private PageCollectorListener listener;

    public PageCollectorTask(Url url, Collector collector, PageCollectorListener listener) {
        this.url = url;
        this.collector = collector;
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            String html = HttpUtils.getBodyFromUrl(url.getUrl());
            List<GameScore> games = collector.collect(html);
            listener.onComplete(games, url);
        } catch (IOException ex) {
            logger.error("Error processing url " + url.getUrl(), ex);
        }
    }

    public void setPageCollectorListener(PageCollectorListener listener) {
        this.listener = listener;
    }
}
