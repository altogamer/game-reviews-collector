/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

import java.util.List;

/**
 * A processor for games.
 * The implementation of this interface is in charge of processing a group of
 * games.
 */
public interface Processor {

    /**
     * Process a group of games.
     * @param games The games to process.
     * @param url The url that was used to fetch this games.
     */
    void process(List<GameScore> games, Url url);
}
