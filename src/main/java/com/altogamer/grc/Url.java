/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

/**
 * General information from an URL.
 */
public class Url {

    public static enum Website {
        DESTRUCTOID,
        GAMESPOT,
        GOG,
        IGN,
        METACRITIC,
        PCGAMER,
        STEAM,
        GAMESRADAR,
        GAMERSGATE,
        VIDEOGAMER
    }

    public static enum Platform {
        PC
    }

    private Platform platform;
    private Website website;
    private String url;

    public Url(Platform platform, Website website, String url) {
        this.platform = platform;
        this.website = website;
        this.url = url;
    }

    public Website getWebsite() {
        return website;
    }

    public void setWebsite(Website website) {
        this.website = website;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
