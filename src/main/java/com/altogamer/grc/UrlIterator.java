/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

/**
 * A iterator that returns url to parse.
 */
public interface UrlIterator {

    /**
     * Resets the iterator to start again.
     */
    void reset();

    /**
     * Returns the next URL to process. If the limit was setted, with empty
     * content will be returned instead.
     * @return the next url to process.
     */
    Url next();

    /**
     * Limits the URL count to process. If the limit is reached, and invalid
     * URL will be returned instead. -1 to disable the limit.
     * @param limit the URL count limit. Defaults to -1 (no limit).
     */
    void setLimit(int limit);

}
