/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Web crawler that scans a group of URL using an specific Collector.
 * This class needs an UrlIterator (that returns URLs to parse), a Collector
 * (that collects games from the URLs), and a GameProcessor (that process the
 * games collected).
 */
public class WebCrawler {

    private static final Logger logger = LoggerFactory.getLogger(WebCrawler.class);

    /** The iterator that returns the web pages to parse. */
    private UrlIterator urlIterator;
    /** The collector for parsing web pages. */
    private Collector collector;
    /** The Processor that will process the games parsed. */
    private Processor processor;

    /* Default Thead Pool Executor values. */
    private int corePoolSize = 15;
    private int maximumPoolSize = 15;
    private int keepAliveTime = 3600;
    private TimeUnit keepAliveTimeUnit = TimeUnit.SECONDS;

    /** Counter that stores collected games count. */
    private int gamesCount;


    /**
     * Collects all games from a website. This method spawns multiple threads to
     * collect games from different URLs, using the Collector to extract games
     * from each web page.
     */
    public void run() throws IOException, InterruptedException {
        gamesCount = 0;
        long startTime = System.currentTimeMillis();
        BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
        final ThreadPoolExecutor pool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, keepAliveTimeUnit, queue);
        urlIterator.reset();

        PageCollectorListener pageCollectorListener = new PageCollectorListener() {
            @Override
            public void onComplete(List<GameScore> gamesCollected, Url url) {
                processor.process(gamesCollected, url);
                logGamesDetails(gamesCollected, url);
                incrementGamesCount(gamesCollected.size());
                //check if there may be more games and schedule next url
                if (gamesCollected.size() > 0) {
                    PageCollectorTask pageCollector = new PageCollectorTask(urlIterator.next(), collector, this);
                    pool.execute(pageCollector);
                }
            }
        };

        //fill pool with initial executors
        for (int i = 0; i < maximumPoolSize; i++) {
            PageCollectorTask pageCollector = new PageCollectorTask(urlIterator.next(), collector, pageCollectorListener);
            pool.execute(pageCollector);
        }

        awaitCompletion(pool);
        pool.shutdown();

        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        logger.info("Finished. {} games collected in {}ms.", gamesCount, totalTime);
    }

    /**
     * Logs a collection of games.
     */
    private void logGamesDetails(List<GameScore> games, Url url) {
        if (logger.isDebugEnabled()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Collected ").append(games.size()).append(" games from ").append(url.getUrl()).append("\n");
            for (GameScore game : games) {
                sb.append(game.toString());
                sb.append("\n");
            }
            logger.debug(sb.toString());
        } else {
            logger.info("Collected {} games from {}", games.size(), url.getUrl());
        }
    }

    /**
     * Blocks until all tasks have completed execution.
     */
    private void awaitCompletion(ThreadPoolExecutor pool) throws InterruptedException {
        while (pool.getTaskCount() != pool.getCompletedTaskCount()) {
            logger.info("Task count: {} - Completed: {}", pool.getTaskCount(), pool.getCompletedTaskCount());
            Thread.sleep(5000);
        }
    }

    private synchronized void incrementGamesCount(int count) {
        gamesCount += count;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public TimeUnit getKeepAliveTimeUnit() {
        return keepAliveTimeUnit;
    }

    public void setKeepAliveTimeUnit(TimeUnit keepAliveTimeUnit) {
        this.keepAliveTimeUnit = keepAliveTimeUnit;
    }

    public Collector getCollector() {
        return collector;
    }

    public void setCollector(Collector collector) {
        this.collector = collector;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public UrlIterator getUrlIterator() {
        return urlIterator;
    }

    public void setUrlIterator(UrlIterator urlIterator) {
        this.urlIterator = urlIterator;
    }

}
