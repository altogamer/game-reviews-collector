/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.Processor;
import com.altogamer.grc.Url;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple Processor that collects all games in an List.
 */
public class SimpleProcessor implements Processor {

    private List<GameScore> games = new ArrayList<>();

    @Override
    public void process(List<GameScore> collectedGames, Url url) {
        games.addAll(collectedGames);
    }

    public List<GameScore> getGames() {
        return games;
    }

}
