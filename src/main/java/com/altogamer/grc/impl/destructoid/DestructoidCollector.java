/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.destructoid;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;

/**
 * Scans and collects game information from an specific game list page
 * from Destructoid.
 */
public class DestructoidCollector implements Collector {

    private static final String URL_PREFIX = "http://www.destructoid.com/";

    /**
     * Returns all the game information from the game list in the page.
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        Jerry doc = jerry(html);
        final List<GameScore> games = new ArrayList<>();
        doc.$("table.products_table tr").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                if (i < 2) {
                    //ignore first two rows: options and separator.
                    return true;
                }
                GameScore game = new GameScore();

                Jerry nameNode = $this.find("div.product_table_product_title");
                game.setName(nameNode.text());
                
                if (game.getName().isEmpty()) {
                    //no game found, must be last row
                    return true;
                }
                
                Jerry urlNode = $this.find("div.product_editorial_links a:first-child");
                game.setUrl(URL_PREFIX + urlNode.attr("href"));

                Jerry scoreNode = $this.find("td:last-child b a");
                game.setScore(parseScore(scoreNode.text()));

                games.add(game);
                return true;
            }
        });
        return games;
    }
    
    private String parseScore(String score) {
        return score.trim().replaceAll("/10", "");
    }
}
