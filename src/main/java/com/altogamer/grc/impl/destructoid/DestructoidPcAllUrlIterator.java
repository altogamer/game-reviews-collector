/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.destructoid;

import com.altogamer.grc.Url;
import com.altogamer.grc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at Destructoid.
 */
public class DestructoidPcAllUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Platform.PC, Url.Website.DESTRUCTOID, "http://www.destructoid.com/products_index.phtml?display=short&filt=reviews&date_s=desc&t=PC&category=PC&name_s=desc&t=PC&score_s=&alpha=&start={index}");
    }

    @Override
    public int getIndexIncrement() {
        return 60;
    }

}
