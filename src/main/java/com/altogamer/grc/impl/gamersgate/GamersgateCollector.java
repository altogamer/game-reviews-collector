/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamersgate;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;
import jodd.util.StringUtil;

/**
 * Scans and collects game information from an specific game list page from
 * Gamersgate.
 */
public class GamersgateCollector implements Collector {

    /**
     * List with invalid end strings for the name of a game. If the name of the
     * game ends withs any of these strings, it is not valid.
     */
    private final String[] SKIP_GAME_CONTAINS_LIST = {
        "2-Pack",
        "3-Pack",
        "4-Pack",
        "Digital Deluxe",
        "DLC Collection",
        "Early Access",
        "Four Pack",
        "Original Soundtrack",
        "+ Soundtrack",
        "Season Pass",
        "Upgrade Pack"
    };

    /**
     * Returns the names, prices and urls from the game list in the page.
     *
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        Jerry doc = jerry(html);
        final List<GameScore> games = new ArrayList<>();
        doc.$("ul.stdlist > li").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                Jerry titleNode = $this.find("a.ttl");
                String name = titleNode.text();
                name = prepareName(name);

                if (isValidName(name)) {
                    GameScore game = new GameScore();
                    game.setName(name);

                    Jerry priceNode = $this.find("div.f_right:nth-child(2) > span");
                    game.setPrice(removeSpecialChars(priceNode.text()));
                    game.setUrl(titleNode.attr("href"));

                    games.add(game);
                }

                return true;
            }

        });
        return games;
    }

    private String prepareName(String name) {
        name = name.trim();
        if (name.endsWith("&nbsp;")) {
            name = StringUtil.replaceLast(name, "&nbsp;", "");
        }
        return name;
    }

    private boolean isValidName(String name) {
        for (String string : SKIP_GAME_CONTAINS_LIST) {
            if (name.contains(string)) {
                return false;
            }
        }
        return true;
    }

    private String removeSpecialChars(String str) {
        return str.replaceAll("\\$", "");
    }
}
