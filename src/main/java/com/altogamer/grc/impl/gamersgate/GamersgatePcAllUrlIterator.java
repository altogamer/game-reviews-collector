/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamersgate;

import com.altogamer.grc.Url;
import com.altogamer.grc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at Gamersgate.
 */
public class GamersgatePcAllUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Platform.PC, Url.Website.GAMERSGATE, "http://www.gamersgate.com/internal-apis/site/product_list?prio=latest-desc&filter=&platform=pc&state=available&q=&pgsize=30&cat_current=&cat_top=&list_style=compact&uri=/games&title=&ajax=1&pg={index}");
    }

    @Override
    public int getIndexIncrement() {
        return 1;
    }

    /**
     * Gamersgate page numbers start at 1.
     */
    @Override
    public void reset() {
        index = 1;
    }
}
