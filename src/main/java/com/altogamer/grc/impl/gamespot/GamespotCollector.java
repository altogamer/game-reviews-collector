/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamespot;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;

/**
 * Scans and collects game information from an specific game list page
 * from Gamespot.
 */
public class GamespotCollector implements Collector {

    private static final String URL_PREFIX = "http://www.gamespot.com";

    /**
     * Returns all the game information from the game list in the page.
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        //Gamespot creates invalid HTML, using href with no quotes. Add the quotes so that Jerry can parse it.
        html = html.replaceAll("<a href=\\/([\\S]*)", "<a href=\"/$1\"");

        Jerry doc = jerry(html);
        final List<GameScore> games = new ArrayList<>();
        doc.$("section.main article.media").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                GameScore game = new GameScore();

                Jerry nameNode = $this.find("div.media-body h3.media-title");

                String name = nameNode.text().trim();
                int lastIndexOfReview = name.lastIndexOf(" Review");
                if (lastIndexOfReview > 0) {
                    name = name.substring(0, name.lastIndexOf(" Review")); //remove the word "review" from title
                }
                game.setName(name);

                Jerry urlNode = $this.find("a:first-child");
                game.setUrl(URL_PREFIX + urlNode.attr("href"));

                Jerry scoreNode = $this.find("div.well--review-gs strong");
                game.setScore(prepareScore(scoreNode.text()));

                games.add(game);
                return true;
            }
        });
        return games;
    }

    private String prepareScore(String parsedScore) {
        try {
            Double score = Double.parseDouble(parsedScore);
            if (score == 0) {
                //GameSpot sometimes has "zero" scores, and its invalid.
                return null;
            }
            return parsedScore;
        }
        catch (NumberFormatException | NullPointerException ex) {
            return null;
        }
    }
}
