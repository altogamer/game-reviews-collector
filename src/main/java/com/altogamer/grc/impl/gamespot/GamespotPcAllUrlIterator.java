/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamespot;

import com.altogamer.grc.Url;
import com.altogamer.grc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at Gamespot.
 */
public class GamespotPcAllUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Platform.PC, Url.Website.GAMESPOT, "http://www.gamespot.com/pc/reviews/?page={index}");
    }

    @Override
    public int getIndexIncrement() {
        return 1;
    }

}
