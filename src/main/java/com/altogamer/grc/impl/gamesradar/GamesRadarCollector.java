/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamesradar;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;
import jodd.util.StringUtil;

/**
 * Scans and collects game information from an specific game list page from
 * Gamespot.
 */
public class GamesRadarCollector implements Collector {

    private static final String URL_PREFIX = "http://www.gamesradar.com";

    /**
     * Returns all the game information from the game list in the page.
     *
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        final List<GameScore> games = new ArrayList<>();
        if (html == null) {
            return games;
        }
        Jerry doc = jerry(html);
        doc.$("li.list_page_item").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                GameScore game = new GameScore();

                Jerry nameNode = $this.find("h2.headline a");

                String name = nameNode.text().trim();
                game.setName(prepareName(name));

                game.setUrl(URL_PREFIX + nameNode.attr("href"));

                Jerry scoreNode = $this.find(".review_stars_small p.stars");
                game.setScore(prepareScore(scoreNode));

                games.add(game);
                return true;
            }
        });
        return games;
    }

    private String prepareName(String name) {
        name = removeAtEnd(name, " review [Update]");
        name = removeAtEnd(name, " review");
        return name;
    }

    private String removeAtEnd(String original, String substring) {
        int lastIndex = original.toLowerCase().lastIndexOf(substring.toLowerCase());
        if (lastIndex > 0) {
            original = original.substring(0, lastIndex);
        }
        return original;
    }

    private String prepareScore(Jerry scoreNode) {
        String cssClass = scoreNode.attr("class");
        if (cssClass != null) {
            String stars = cssClass.split(" ")[1];
            return StringUtil.remove(stars, "stars");
        }
        return null;
    }

}
