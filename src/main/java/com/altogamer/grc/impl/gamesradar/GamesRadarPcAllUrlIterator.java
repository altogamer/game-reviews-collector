/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamesradar;

import com.altogamer.grc.Url;
import com.altogamer.grc.UrlIterator;

/**
 * Iterator for all PC games at GamesRadar.
 */
public class GamesRadarPcAllUrlIterator implements UrlIterator {

    private int index = 1;
    private int limit = -1;

    public Url createBaseUrl() {
        return new Url(Url.Platform.PC, Url.Website.GAMESRADAR, "http://www.gamesradar.com/pc/reviews/list/?page=");
    }

    @Override
    public void reset() {
        index = 1;
    }

    @Override
    public Url next() {
        if (limit == 0 || (limit > -1 && index >= limit)) {
            index = 9999;
        }
        Url baseUrl = createBaseUrl();
        if (index >= 2) {
            //page 1 must not include the page number because GamesRadar forces a redirect.
            baseUrl.setUrl(baseUrl.getUrl() + index);
        }
        index++;
        return baseUrl;
    }

    @Override
    public void setLimit(int limit) {
        this.limit = limit;
    }


}
