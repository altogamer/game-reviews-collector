/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gog;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import jodd.util.StringUtil;

/**
 * Scans and collects game information from an specific game list page from GOG
 * (Good Old Games).
 */
public class GogCollector implements Collector {

    private static final String URL_PREFIX = "http://www.gog.com";

    /**
     * Returns the names, prices and urls from the game list in the page.
     *
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String json) throws IOException {
        final List<GameScore> games = new ArrayList<>();
        if (json.isEmpty()) {
            return games;
        }

        ObjectMapper mapper = new ObjectMapper();
        Iterator<JsonNode> elements = mapper.readTree(json).get("products").elements();

        while (elements.hasNext()) {
            JsonNode element = elements.next();
            if (!element.get("isGame").asBoolean()) {
                continue;
            }
            GameScore game = new GameScore();

            game.setName(prepareName(element.get("title").asText()));
            if (isInvalidGame(game.getName())) {
                continue;
            }
            game.setPrice(getPrice(element.get("price")));
            game.setUrl(URL_PREFIX + element.get("url").asText());
            game.setScore(element.get("rating").asText());

            games.add(game);
        }

        return games;
    }

    private String prepareName(String name) {
        name = name.trim();
        name = removeSpecialChars(name);
        if (name.endsWith(", The")) {
            name = name.substring(0, name.lastIndexOf(", The"));
            name = "The " + name;
        }
        return name;
    }

    private String removeSpecialChars(String str) {
        return str.replaceAll("[®™]", "");
    }

    private String getPrice(JsonNode price) {
        if (price.get("isFree").asBoolean()) {
            return "0";
        }
        else {
            return price.get("amount").asText();
        }
    }

    private boolean isInvalidGame(String name) {
        return name.startsWith("DLC: ");
    }

}
