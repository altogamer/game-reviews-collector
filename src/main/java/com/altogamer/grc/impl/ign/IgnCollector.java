/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.ign;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;

/**
 * Scans and collects game information from an specific game list page
 * from IGN.
 */
public class IgnCollector implements Collector {

    private static final String URL_PREFIX = "http://www.ign.com";

    /**
     * Returns all the game information from the game list in the page.
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        Jerry doc = jerry(html);
        final List<GameScore> games = new ArrayList<>();
        doc.$("div#item-list > div.itemList > div.itemList-item").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                GameScore game = new GameScore();

                Jerry nameNode = $this.find("div.item-title h3 a");
                game.setName(nameNode.text().trim());
                game.setUrl(URL_PREFIX + nameNode.attr("href"));

                Jerry scoreNode = $this.find("div.scoreBox span.scoreBox-score");
                game.setScore(scoreNode.text());

                games.add(game);
                return true;
            }
        });
        return games;
    }
}
