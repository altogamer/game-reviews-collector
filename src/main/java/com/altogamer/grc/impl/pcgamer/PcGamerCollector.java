/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.pcgamer;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;

/**
 * Scans and collects game information from an specific game list page from
 * Destructoid.
 */
public class PcGamerCollector implements Collector {

    private static final String URL_PREFIX = "http://www.pcgamer.com";

    /**
     * Returns all the game information from the game list in the page.
     *
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        Jerry doc = jerry(html);
        final List<GameScore> games = new ArrayList<>();
        doc.$(".article_box .headline a").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                GameScore game = new GameScore();
                Jerry urlNode = $this;
                game.setUrl(URL_PREFIX + urlNode.attr("href"));

                String name = parseName($this.find("h3").text().trim());
                game.setName(name);

//                Jerry scoreNode = $this.find("td.score");
//                String score = scoreNode.text().trim();
//                game.setScore(score);

                if (isValidName(game.getName())) {
                    games.add(game);
                }
                return true;
            }
        });
        return games;
    }

    private String parseName(String name) {
        name = removeAtEnd(name, " review");
        return name;
    }

    private boolean isValidName(String name) {
        //PC Gamer contains reviews of hardware, skip them
        name = name.toLowerCase();
        return !name.contains("nvidia ") && !name.contains("geforce ") && !name.contains("amd ") && !name.contains("radeon ");
    }

    private String removeAtEnd(String original, String substring) {
        int lastIndex = original.toLowerCase().lastIndexOf(substring.toLowerCase());
        if (lastIndex > 0) {
            original = original.substring(0, lastIndex);
        }
        return original;
    }

}
