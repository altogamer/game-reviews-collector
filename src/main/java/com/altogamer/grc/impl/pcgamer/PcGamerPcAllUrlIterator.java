/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.pcgamer;

import com.altogamer.grc.Url;
import com.altogamer.grc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at PC Gamer.
 */
public class PcGamerPcAllUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Platform.PC, Url.Website.PCGAMER, "http://www.pcgamer.com/reviews/?page={index}");
    }

    @Override
    public void reset() {
        index = 1;
    }

    @Override
    public int getIndexIncrement() {
        return 1;
    }

}
