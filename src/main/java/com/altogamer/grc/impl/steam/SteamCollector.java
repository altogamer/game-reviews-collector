/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.steam;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;
import jodd.util.StringUtil;

/**
 * Scans and collects game information from an specific game list page from
 * Steam.
 */
public class SteamCollector implements Collector {

    /**
     * Returns the names, prices and urls from the game list in the page.
     *
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        Jerry doc = jerry(html);
        final List<GameScore> games = new ArrayList<>();
        doc.$("a.search_result_row").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                GameScore game = new GameScore();

                Jerry nameNode = $this.find(".search_name .title");
                game.setName(removeSpecialChars(nameNode.text()));

                game.setUrl($this.attr("href"));

                Jerry priceNode = $this.find(".search_price");
                game.setPrice(defaultIfBlank(priceNode.text(), null));

                games.add(game);
                return true;
            }
        });
        return games;
    }

    private String removeSpecialChars(String str) {
        return str.replaceAll("[®™]", "").trim();
    }

    private String defaultIfBlank(String s, String defaultStr) {
        if (StringUtil.isBlank(s)) {
            return defaultStr;
        }
        if (s.equals("Free to Play") || s.equals("Free")) {
            return "0";
        }
        int index = s.indexOf("$", 1);
        if (index == -1) {
            return s.substring(1).trim();
        } else {
            return s.substring(index + 1).trim();
        }
    }

}
