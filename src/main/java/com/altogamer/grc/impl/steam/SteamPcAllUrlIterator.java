/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.steam;

import com.altogamer.grc.Url;
import com.altogamer.grc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at Steam.
 */
public class SteamPcAllUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Platform.PC, Url.Website.STEAM, "http://store.steampowered.com/search/?sort_by=Released&sort_order=DESC&category1=998&page={index}");
    }

    @Override
    public int getIndexIncrement() {
        return 1;
    }

}
