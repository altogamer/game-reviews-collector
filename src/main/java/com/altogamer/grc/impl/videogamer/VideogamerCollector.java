/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.videogamer;

import com.altogamer.grc.Collector;
import com.altogamer.grc.GameScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import static jodd.jerry.Jerry.jerry;
import jodd.jerry.JerryFunction;

/**
 * Scans and collects game information from an specific game list page
 * from Videogamer.
 */
public class VideogamerCollector implements Collector {

    private static final String URL_PREFIX = "http://www.videogamer.com";

    /**
     * Returns all the game information from the game list in the page.
     * @return a List of GameScore, empty if no games were found.
     */
    @Override
    public List<GameScore> collect(String html) throws IOException {
        Jerry doc = jerry(html);
        final List<GameScore> games = new ArrayList<>();
        doc.$(".resultList .itemReview.itemGame").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $this, int i) {
                GameScore game = new GameScore();
                Jerry urlNode = $this.find(".details h2 a");
                game.setUrl(URL_PREFIX + urlNode.attr("href"));

                String name = parseName(urlNode.text().trim());
                game.setName(name);

                Jerry scoreNode = $this.find(".scoreBox a");
                String score = scoreNode.text().trim();
                game.setScore(score);

                games.add(game);

                return true;
            }
        });
        return games;
    }

    private String parseName(String name) {
        name = removeAtEnd(name, " review");
        name = name.replaceAll("&amp;", "&");
        return name;
    }

    private String removeAtEnd(String original, String substring) {
        int lastIndex = original.toLowerCase().lastIndexOf(substring.toLowerCase());
        if (lastIndex > 0) {
            original = original.substring(0, lastIndex);
        }
        return original;
    }
}
