/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.videogamer;

import com.altogamer.grc.Url;
import com.altogamer.grc.impl.AbstractUrlIterator;

/**
 * Iterator for all PC games at Videogamer.
 */
public class VideogamerPcAllUrlIterator extends AbstractUrlIterator {

    @Override
    public Url createBaseUrl() {
        return new Url(Url.Platform.PC, Url.Website.VIDEOGAMER, "http://www.videogamer.com/reviews/pc/?page={index}");
    }

    @Override
    public int getIndexIncrement() {
        return 1;
    }

    @Override
    public void reset() {
        index = 1;
    }

}
