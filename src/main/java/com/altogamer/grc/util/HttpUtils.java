/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.util;

import java.io.IOException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

/**
 * General utilities for handling HTML.
 */
public class HttpUtils {

    /**
     * Returns the content of a URL as String.
     *
     * @param url the URL to fetch.
     * @return the content of the URL as String.
     */
    public static String getBodyFromUrl(String url) throws IOException {
        HttpResponse result = HttpRequest.get(url)
                .header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0")
                .send();
        return result.statusCode() == 200 ? result.bodyText() : "";
    }
}
