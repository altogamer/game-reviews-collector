/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.destructoid;

import com.altogamer.grc.UrlIterator;
import com.altogamer.grc.Url;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class DestructoidPcAllUrlIteratorTest {

    private UrlIterator iterator;

    @Before
    public void setup() {
        iterator = new DestructoidPcAllUrlIterator();
    }

    @Test
    public void setLimit_limitDefault_returnsManyUrl() {
        for (int i = 0; i < 600; i += 60) {
            Url url = iterator.next();
            assertTrue(url.getUrl().endsWith(String.valueOf(i)));
        }
    }

    @Test
    public void setLimit_zeroPages_returnsInvalidUrl() {
        iterator.setLimit(0);
        Url url = iterator.next();
        assertTrue(url.getUrl().endsWith("9999"));
    }

    @Test
    public void setLimit_somePages_triggersLimit() {
        iterator.setLimit(3);
        Url url;
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("0"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("60"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("120"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("9999"));
        url = iterator.next();
        assertTrue(url.getUrl().endsWith("9999"));
    }

}
