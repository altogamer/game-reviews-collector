/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamersgate;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class GamersgateCollectorTest {

    private String getHtmlWithGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gamersgate.com/internal-apis/site/product_list?prio=latest-desc&filter=&platform=pc&state=available&q=&pgsize=30&cat_current=&cat_top=&list_style=compact&uri=/games&title=&ajax=1&pg=165");
    }

    private String getHtmlWithEmptyGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gamersgate.com/internal-apis/site/product_list?prio=latest-desc&filter=&platform=pc&state=available&q=&pgsize=30&cat_current=&cat_top=&list_style=compact&uri=/games&title=&ajax=1&pg=9999");
    }

    private boolean isValidPrice(String price) {
        try {
            Double.parseDouble(price);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    @Test
    public void collect_pageWithGames_returnsGames() throws Exception {
        GamersgateCollector instance = new GamersgateCollector();
        List<GameScore> games = instance.collect(getHtmlWithGameList());
        assertNotNull(games);
        for (GameScore gameScore : games) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.getUrl());
            assertNotNull(gameScore.getName());
            assertTrue(isValidPrice(gameScore.getPrice()));
        }
        assertEquals(27, games.size());
    }

    @Test
    public void collect_pageWithNoGames_returnsEmptyList() throws Exception {
        GamersgateCollector instance = new GamersgateCollector();
        List<GameScore> games = instance.collect(getHtmlWithEmptyGameList());
        assertNotNull(games);
        assertTrue(games.isEmpty());
    }

}