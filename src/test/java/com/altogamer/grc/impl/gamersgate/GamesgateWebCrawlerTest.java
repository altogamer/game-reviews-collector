/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamersgate;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.WebCrawler;
import com.altogamer.grc.impl.SimpleProcessor;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

public class GamesgateWebCrawlerTest {

    @Test
    @Ignore
    public void run_defaultSettings_returnsGames() throws Exception {
        SimpleProcessor processor = new SimpleProcessor();
        GamersgatePcAllUrlIterator urlIterator = new GamersgatePcAllUrlIterator();
//        urlIterator.setLimit(5);

        WebCrawler crawler = new WebCrawler();
        crawler.setCollector(new GamersgateCollector());
        crawler.setProcessor(processor);

        crawler.setUrlIterator(urlIterator);

        crawler.setCorePoolSize(5);
        crawler.setMaximumPoolSize(5);

        crawler.run();

        assertNotNull(processor.getGames());
        assertTrue(processor.getGames().size() > 0);

        for (GameScore gameScore : processor.getGames()) {
            System.out.println(gameScore.toString());
        }
    }

}
