/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamespot;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class GamespotCollectorTest {

    private String getHtmlWithGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gamespot.com/pc/reviews/?page=1");
    }

    private String getHtmlWithEmptyGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gamespot.com/pc/reviews/?page=9999");
    }

    private boolean isValidScore(String score) {
        try {
            Integer.parseInt(score);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    @Test
    public void collect_pageWithGames_returnsGames() throws Exception {
        GamespotCollector instance = new GamespotCollector();
        List<GameScore> games = instance.collect(getHtmlWithGameList());
        assertNotNull(games);
        assertEquals(24, games.size());
        for (GameScore gameScore : games) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.getName());
            assertNotNull(gameScore.getUrl());
            assertFalse(gameScore.getUrl().endsWith("null"));
            assertNotNull(gameScore.getScore());
            assertTrue(isValidScore(gameScore.getScore()));
        }
    }

    @Test
    public void collect_pageWithNoGames_returnsEmptyList() throws Exception {
        GamespotCollector instance = new GamespotCollector();
        List<GameScore> games = instance.collect(getHtmlWithEmptyGameList());
        assertNotNull(games);
        assertTrue(games.isEmpty());
    }

}
