/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamespot;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.WebCrawler;
import com.altogamer.grc.impl.SimpleProcessor;
import org.apache.commons.validator.routines.UrlValidator;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

public class GamespotWebCrawlerTest {

    @Test
    @Ignore
    public void run_defaultSettings_returnsGames() throws Exception {
        SimpleProcessor processor = new SimpleProcessor();
        GamespotPcAllUrlIterator urlIterator = new GamespotPcAllUrlIterator();
//        urlIterator.setLimit(5);

        WebCrawler crawler = new WebCrawler();
        crawler.setCollector(new GamespotCollector());
        crawler.setProcessor(processor);

        crawler.setUrlIterator(urlIterator);

        crawler.setCorePoolSize(5);
        crawler.setMaximumPoolSize(5);

        crawler.run();

        assertNotNull(processor.getGames());
        assertTrue(processor.getGames().size() > 0);

        UrlValidator urlValidator = new UrlValidator();
        for (GameScore gameScore : processor.getGames()) {
            System.out.println(gameScore.toString());
            assertTrue("Url must be valid", urlValidator.isValid(gameScore.getUrl()));
        }
    }

}
