/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gamesradar;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class GamesRadarCollectorTest {

    private String getHtmlWithGameListInFirstPage() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gamesradar.com/pc/reviews/list/?page=");
    }

    private String getHtmlWithGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gamesradar.com/pc/reviews/list/?page=2");
    }

    private String getHtmlWithEmptyGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gamesradar.com/pc/reviews/list/?page=9999");
    }

    @Test
    public void collect_pageWithGamesInFirstPage_returnsGames() throws Exception {
        GamesRadarCollector instance = new GamesRadarCollector();
        List<GameScore> games = instance.collect(getHtmlWithGameListInFirstPage());
        assertNotNull(games);
        for (GameScore gameScore : games) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.getUrl());
            assertNotNull(gameScore.getName());
        }
        assertEquals(40, games.size());
    }

    @Test
    public void collect_pageWithGamesInNotFirstPage_returnsGames() throws Exception {
        GamesRadarCollector instance = new GamesRadarCollector();
        List<GameScore> games = instance.collect(getHtmlWithGameList());
        assertNotNull(games);
        for (GameScore gameScore : games) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.getUrl());
            assertNotNull(gameScore.getName());
        }
        assertEquals(40, games.size());
    }


    @Test
    public void collect_pageWithNoGames_returnsEmptyList() throws Exception {
        GamesRadarCollector instance = new GamesRadarCollector();
        List<GameScore> games = instance.collect(getHtmlWithEmptyGameList());
        assertNotNull(games);
        assertTrue(games.isEmpty());
    }

}