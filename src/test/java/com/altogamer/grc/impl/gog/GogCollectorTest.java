/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.gog;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class GogCollectorTest {


    private String getHtmlWithGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gog.com/games/ajax/filtered?mediaType=game&page=1&sort=bestselling&system=windows_7,windows_8,windows_vista,windows_xp");
    }

    private String getHtmlWithEmptyGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.gog.com/games/ajax/filtered?mediaType=game&page=9999&sort=bestselling&system=windows_7,windows_8,windows_vista,windows_xp");
    }

    private boolean isValidPrice(String price) {
        try {
            Double.parseDouble(price);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    @Test
    public void collect_pageWithGames_returnsGames() throws Exception {
        GogCollector instance = new GogCollector();
        List<GameScore> games = instance.collect(getHtmlWithGameList());
        assertNotNull(games);
        assertEquals(50, games.size());
        for (GameScore gameScore : games) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.getName());
            assertNotNull(gameScore.getUrl());
            assertTrue(isValidPrice(gameScore.getPrice()));
        }
    }

    @Test
    public void collect_pageWithNoGames_returnsEmptyList() throws Exception {
        GogCollector instance = new GogCollector();
        List<GameScore> games = instance.collect(getHtmlWithEmptyGameList());
        assertNotNull(games);
        assertTrue(games.isEmpty());
    }

}