/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.metacritic;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class MetacriticCollectorTest {

    private String getHtmlWithGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.metacritic.com/browse/games/release-date/available/pc/name?view=detailed&page=0");
    }

    private String getHtmlWithEmptyGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://www.metacritic.com/browse/games/release-date/available/pc/name?view=detailed&page=9999");
    }

    private boolean isValidScore(String score) {
        if (score.equals("tbd")) {
            return true;
        }
        try {
            Integer.parseInt(score);
            return true;
        }
        catch (NumberFormatException ex) {
            return false;
        }
    }

    @Test
    public void collect_pageWithGames_returnsGames() throws Exception {
        MetacriticCollector instance = new MetacriticCollector();
        List<GameScore> games = instance.collect(getHtmlWithGameList());
        assertNotNull(games);
        assertEquals(100, games.size());
        for (GameScore gameScore : games) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.getName());
            assertNotNull(gameScore.getUrl());
            assertNotNull(gameScore.getReleaseDate());
            assertNotNull(gameScore.getScore());
            //assertTrue(StringUtil.isNotBlank(gameScore.getPublisher()));
            assertTrue(isValidScore(gameScore.getScore()));
        }
    }

    @Test
    public void collect_pageWithNoGames_returnsEmptyList() throws Exception {
        MetacriticCollector instance = new MetacriticCollector();
        List<GameScore> games = instance.collect(getHtmlWithEmptyGameList());
        assertNotNull(games);
        assertTrue(games.isEmpty());
    }

}