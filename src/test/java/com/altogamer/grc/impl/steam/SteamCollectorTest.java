/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.grc.impl.steam;

import com.altogamer.grc.GameScore;
import com.altogamer.grc.util.HttpUtils;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class SteamCollectorTest {

    private String getHtmlWithGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://store.steampowered.com/search/?sort_by=Released&sort_order=DESC&category1=998&page=20");
    }

    private String getHtmlWithEmptyGameList() throws IOException {
        return HttpUtils.getBodyFromUrl("http://store.steampowered.com/search/?sort_by=Released&sort_order=DESC&category1=998&page=9999");
    }

    private boolean isValidPrice(String price) {
        try {
            Double.parseDouble(price);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    @Test
    public void collect_pageWithGames_returnsGames() throws Exception {
        SteamCollector instance = new SteamCollector();
        List<GameScore> games = instance.collect(getHtmlWithGameList());
        assertNotNull(games);
        assertEquals(25, games.size());
        for (GameScore gameScore : games) {
            System.out.println(gameScore.toString());
            assertNotNull(gameScore.getUrl());
            assertNotNull(gameScore.getName());
            assertNotNull(isValidPrice(gameScore.getPrice()));
        }
    }

    @Test
    public void collect_pageWithNoGames_returnsEmptyList() throws Exception {
        SteamCollector instance = new SteamCollector();
        List<GameScore> games = instance.collect(getHtmlWithEmptyGameList());
        assertNotNull(games);
        assertTrue(games.isEmpty());
    }

}